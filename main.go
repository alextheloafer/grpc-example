package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"

	"gitlab.com/alextheloafer/grpc-example/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Servicer struct {
	service.UnimplementedAPIServer
}

func (s *Servicer) Request(ctx context.Context, data *service.Data) (*service.Reply, error) {
	if data.GetId() == 0 {
		return nil, errors.New("id is 0")
	}
	msg := string(data.GetValue())
	if msg == "" {
		return &service.Reply{Success: false, Message: "message is empty"}, nil
	}

	return &service.Reply{Success: true, Message: msg}, nil
}

func (s *Servicer) Stream(stream service.API_StreamServer) error {
	for {
		data, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		for i := 1; i <= int(data.GetCount()); i++ {
			if err := stream.Send(&service.Reply{Success: true, Message: fmt.Sprintf("counter %d", i)}); err != nil {
				return err
			}
		}
	}
}

func main() {
	var address string
	flag.StringVar(&address, "address", "localhost:20100", "listener address")
	flag.Parse()

	log.Printf("opening connection at %s\n", address)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatal(err)
	}

	grpcServer := grpc.NewServer()
	reflection.Register(grpcServer)
	service.RegisterAPIServer(grpcServer, new(Servicer))
	log.Fatal(grpcServer.Serve(lis))
}
