FROM golang:1.15-alpine3.12 as builder

WORKDIR /app/src

COPY . .

RUN go get ./...

RUN mkdir -p /app/build && go build -o /app/build/server main.go

FROM alpine:3.12

WORKDIR /app

COPY --from=builder /app/build/server /app

CMD ["/app/server"]